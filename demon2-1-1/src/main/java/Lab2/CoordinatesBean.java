package Lab2;

import java.util.HashMap;
import java.util.LinkedList;

public class CoordinatesBean {
    private final HashMap<User, LinkedList<Coordinates>> coordinates = new HashMap<>();

    public CoordinatesBean(){}

    public void add(User user, Coordinates coordinates) {
		if (!this.coordinates.containsKey(user)) {
			this.coordinates.put(user, new LinkedList<>());
		}
        this.coordinates.get(user).add(coordinates);
    }

    public LinkedList<Coordinates> getPoints(User user) {
		if (coordinates.containsKey(user))
			return coordinates.get(user);
		else
			return new LinkedList<>();
    }

}