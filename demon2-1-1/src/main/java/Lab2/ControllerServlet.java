package Lab2;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/controller"})
public class ControllerServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String x = req.getParameter("x");
        String y = req.getParameter("y");
        String r = req.getParameter("r");

        if (x == null || y == null || r == null) {
            req.getServletContext()
                    .getRequestDispatcher("/index.jsp")
                    .forward(req, resp);
        } else {
            req.getServletContext()
                    .getRequestDispatcher("/check")
                    .forward(req, resp);
        }
    }
}
