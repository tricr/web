package Lab2;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = {"/check"})
public class AreaCheckServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Double x = null;
        Double y = null;
        Double r = null;

        CoordinatesBean coordinatesBean = (CoordinatesBean) request.getServletContext().
                getAttribute("CoordinatesBean");
        if(coordinatesBean == null){
            coordinatesBean = new CoordinatesBean();
        }

        try {
            x = Double.parseDouble(request.getParameter("x").trim());
        } catch (NumberFormatException e) {
            request.getRequestDispatcher("/controller")
                    .forward(request, response);
        }

        try {
            y = Double.parseDouble(request.getParameter("y").trim().replaceAll(",", "."));
        } catch (NumberFormatException e) {
            request.getRequestDispatcher("/controller")
                    .forward(request, response);
        }
        try {
            r = Double.parseDouble(request.getParameter("r").trim().replaceAll(",", "."));
        } catch (NumberFormatException e) {
            request.getRequestDispatcher("/controller")
                    .forward(request, response);
        }

        try (PrintWriter out = response.getWriter()) {
            if (x != null && y != null && r != null) {
				User user = User.fromRequest(request);
                Coordinates coordinates = new Coordinates(user, x, y, r);
                coordinatesBean.add(user, coordinates);
                request.getServletContext().setAttribute("CoordinatesBean", coordinatesBean);

                out.println(coordinates.toJSON());
            }
        }
    }
}
