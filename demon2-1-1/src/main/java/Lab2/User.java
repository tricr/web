package Lab2;

import jakarta.servlet.http.HttpServletRequest;

public class User {

	private String username;

	public String getUsername() {
		return username;
	}

	public User(String username) {
		this.username = username;
	}

	public static User fromRequest(HttpServletRequest request) {
		String username = request.getUserPrincipal().getName();

		return new User(username);
	}

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof User) && ((User) obj).username.equals(username);
	}

	@Override
	public int hashCode() {
		return username.hashCode();
	}
}
