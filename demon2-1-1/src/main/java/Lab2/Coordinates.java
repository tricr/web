package Lab2;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Coordinates {

	private User user;
    private double x;
    private double y;
    private double r;
    private boolean hit;
    private String currentTime;
    private Double executionTimeMS;

	public User getUser() {
		return user;
	}

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getR() {
        return r;
    }

    public boolean isHit() {
        return hit;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public double getExecutionTimeMS() {
        return executionTimeMS;
    }

    private boolean check(double x, double y, double r) {
        if (x <= 0 && y <= 0) {
            return (x*x + y*y <= r*r);
        }
        if (x >= 0 && y >= 0) {
            return (-y >= x - r/2);
        }
        if (x >= 0 && y <= 0) {
            return (x <= r/2 && y >= -r );
        }

        return false;
    }

    public Coordinates( User user, double x, double y, double r) {
		this.user = user;
        this.x = x;
        this.y = y;
        this.r = r;

        long startTime = System.nanoTime();

        hit = check(x, y, r);

        currentTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());

        executionTimeMS = ((double) System.nanoTime() - startTime) / Math.pow(10, 6);
    }

    public String toJSON() {
        return "{" +
                " \"x\":" + x +
                ", \"y\":" + y +
                ", \"r\":" + r +
                ", \"hit\":" + hit +
                ", \"currentTime\":\"" + currentTime + '\"' +
                ", \"executionTimeMS\":" + executionTimeMS +
                '}';
    }

}

