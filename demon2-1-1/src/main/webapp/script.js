$(document).ready(function () {

    function changeR()
    {
        checkR();
        $('.r2p').html(+r/2);
        $('.r2n').html(-(r/2));
        $('.rp').html(+r);
        $('.rn').html(-r);
    }

    let x, y, r;
    let yError = document.getElementById("yError");

    changeR();
    $('select[name=R]').change(function () {
        changeR();
    });


    function checkX() {
        x = $('input[name=X]:checked').val();
        if (x == undefined || x === "")
        {
            yError.textContent = "Поставьте значение X";
            return false;
        }
        return true;
    }

    function checkY() {
        y = $('input[name=Y]').val().replace(",", ".");
        if (y == undefined || y === "")
        {
            yError.textContent = "Введите значение Y";
            return false;
        }
        else if (!$.isNumeric(y))
        {
            yError.textContent = "Y - не число";
            return false;
        }
        else if (!(!isNaN(parseFloat(y)) && isFinite(y)))
        {
            yError.textContent = "Y - не число";
            return false;
        }
        else if (y <= -5 || y >= 3)
        {
            yError.textContent = "Y вне диапазона (-5;3)";
            return false;
        }
        else
        {
            yError.textContent = "";
            return true;
        }
        return true;
    }

    function checkR() {
        r = $('select[name=R] option:selected').val()
        return true;
    }

    function addRow(data) {
        $('#resultTable tbody').prepend(`
			<tr class="${data.hit ? "hit" : "lose"}">
			<td>${data.x}</td>
			<td>${data.y}</td>
			<td>${data.r}</td>
			<td>${data.hit ? "Да" : "Нет"}</td>
			<td>${data.executionTimeMS}</td>
			<td>${data.currentTime}</td>
			</tr>
		`);
        let point = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
        point.setAttribute("cx", data.x*$('#area').attr('width')/(2.5*r) + $('#area').attr('width') / 2);
        point.setAttribute("cy", -data.y*$('#area').attr('height')/(2.5*r) + $('#area').attr('height') / 2);
        point.setAttribute("r", 5);
        if (data.hit)
            point.setAttribute("fill", 'black');
        else
            point.setAttribute("fill", 'white');
        $('#area').append(point);
    }



    $("#checkForm").on('submit', function (e) {
        e.preventDefault();
        if (checkX() && checkY() && checkR()) {
            sendAjax();
        }
    });


    $('#area').click(function (e) {
        const X = (e.clientX - this.getBoundingClientRect().left) - $('#area').width() / 2;
        const Y = $('#area').height() / 2 - (e.clientY - this.getBoundingClientRect().top);
        x = (r * X / ($('#area').width() / 2.5)).toPrecision(6);
        y = (r * Y / ($('#area').height() / 2.5)).toPrecision(6);
        sendAjax();
    });

    function sendAjax() {
        $.ajax({
            type: "POST",
            url: "controller",
            data: {
                "x": x,
                "y": y,
                "r": r,
            },
            success: addRow,
            dataType: "json"
        });
    }


    $("#reset").on('click', function (e) {
        $.ajax({
            type: "GET",
            url: "clear",
            success: function () {
                $('#resultTable tbody').html("");
                $('#area>circle').remove()
            }
        });
    });




});
$("input:checkbox").on('click', function() {
    let $box = $(this);
    if ($box.is(":checked")) {
        let group = "input:checkbox[name='" + $box.attr("name") + "']";
        $(group).prop("checked", false);
        $box.prop("checked", true);
    } else {
        $box.prop("checked", false);
    }
});