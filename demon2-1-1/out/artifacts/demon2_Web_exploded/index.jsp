<%@ page import="Lab2.Coordinates" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ListIterator" %>
<jsp:useBean id='CoordinatesBean' scope='session' class='Lab2.CoordinatesBean' />
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>ТРИЦР 2</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<header>
    <div class = "twoBlocks shadow">
        <div class="block">
            <div> ТРИЦР <br>  Лабораторная работа 2 <br> Вариант 51262 </div>
        </div>
        <div class="block max">
            <div> Трофимов Анатолий <br> Разумов Андрей <br> P33222</div>
        </div>
    </div>
</header>
<div class = "twoBlocks">
    <div class="block">
        <div class="areaWrap">
            <svg id="area" width="1000" height="1000" viewBox="0 0 1000 1000">
                <!-- Triangle -->
                <polygon points="500,500 500,300 700,500" fill="#4d4d4d"/>

                <!-- Square -->
                <polygon points="500,500 500,900 700,900 700,500" fill="#4d4d4d"/>

                <!-- Circle part -->
                <mask id="circlePart" maskUnits="userSpaceOnUse" x="100" y="500" width="400" height="400"><circle cx="500" cy="500" r="400" fill="#FFFFFF"/></mask>
                <rect x="100" y="500" width="400" height="400" fill="#4d4d4d" mask="url(#circlePart)"/>

                <!-- X and Y lines -->
                <line x1="500" y1="0" x2="500" y2="1000" stroke-width=4 stroke="white"/>
                <line x1="1000" y1="500" x2="0" y2="500" stroke-width=4 stroke="white"/>

                <!-- Arrow Y -->
                <line x1="500" y1="0" x2="490" y2="24" stroke-width=4 stroke="white"/>
                <line x1="510" y1="24" x2="500" y2="0" stroke-width=4 stroke="white"/>

                <!-- Arrow X -->
                <line x1="1000" y1="500" x2="976" y2="490" stroke-width=4 stroke="white"/>
                <line x1="976" y1="510" x2="1000" y2="500" stroke-width=4 stroke="white"/>

                <!-- Short marks -->
                <line x1="700" y1="490" x2="700" y2="510" stroke-width=8 stroke="white"/>
                <line x1="490" y1="300" x2="510" y2="300" stroke-width=8 stroke="white"/>
                <line x1="300" y1="490" x2="300" y2="510" stroke-width=8 stroke="white"/>
                <line x1="510" y1="700" x2="490" y2="700" stroke-width=8 stroke="white"/>

                <!-- Long marks -->
                <line x1="900" y1="480" x2="900" y2="520" stroke-width=8 stroke="white"/>
                <line x1="480" y1="100" x2="520" y2="100" stroke-width=8 stroke="white"/>
                <line x1="100" y1="480" x2="100" y2="520" stroke-width=8 stroke="white"/>
                <line x1="480" y1="900" x2="520" y2="900" stroke-width=8 stroke="white"/>

                <!-- X Y -->
                <text x="980" y="480"  fill="white" font-family="calibri" font-size="30">X</text>
                <text x="530" y="20"  fill="white" font-family="calibri" font-size="30">Y</text>

                <!-- Y values -->
                <text x="530" y="120" class="rp" fill="white" font-family="calibri" font-size="40">R</text>
                <text x="530" y="310" class="r2p" fill="white" font-family="calibri" font-size="40">R/2</text>
                <text x="530" y="708" class="r2n" fill="white" font-family="calibri" font-size="40">-R/2</text>
                <text x="530" y="915" class="rn" fill="white" font-family="calibri" font-size="40">-R</text>

                <!-- X values -->
                <text x="80" y="470" class="rn" fill="white" font-family="calibri" font-size="40">-R</text>
                <text x="270" y="470" class="r2n" fill="white" font-family="calibri" font-size="40">-R/2</text>
                <text x="670" y="470" class="r2p" fill="white" font-family="calibri" font-size="40">R/2</text>
                <text x="890" y="470" class="rp" fill="white" font-family="calibri" font-size="40">R</text>
            </svg>
        </div>
        <form action="/check" method="post" id="checkForm">
            <div class = "twoBlocks">
                <div class="block chooseBox">
                    Выберите X:
                </div>
                <div class="block chooseBox max">
                        <label>
                            <input type="checkbox" name="X" value="-4" />
                            -4</label>

                        <label>
                            <input type="checkbox" name="X" value="-3" />-3
                        </label>

                        <label>
                            <input type="checkbox" name="X" value="-2"/>
                            -2</label>

                        <label>
                            <input type="checkbox" name="X" value="-1"/>
                            -1</label>

                        <label>
                            <input type="checkbox" name="X" value="0"/>0
                        </label>

                        <label>
                            <input type="checkbox" name="X" value="1"/>
                            1</label>

                        <label>
                            <input type="checkbox" name="X" value="2"/>
                            2</label>

                        <label>
                            <input type="checkbox" name="X" value="3"/>
                            3</label>
                        <label>
                            <input type="checkbox" name="X" value="4"/>
                        4</label>
                </div>
            </div>

            <div class = "twoBlocks">
                <div class="block chooseBox">
                    Введите Y:
                </div>
                <div class="block chooseBox max">
                    <input type="text" name="Y" placeholder="-5 .. 3">
                </div>
            </div>
            <div class = "twoBlocks">
                <div class="block chooseBox">
                    Выберите R:
                </div>
                <div class="block chooseBox max">
                    <select name="R">
                        <option value="1" selected="selected">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>

            <input id="submit" name="submit" type="submit" value="Отправить">
            <input id="reset" name="reset" type="button" value="Сброс">
        </form>

        <p class="error" id="yError"></p>
    </div>
    <div class="block max">
        <table id="resultTable">
            <div class="resultBox"><h2> Результаты проверки </h2></div>
            <thead>
            <tr>
                <th>X</th>
                <th>Y</th>
                <th>R</th>
                <th>Попадание</th>
                <th>Время выполнения</th>
                <th>Текущее время</th>
            </tr>
            </thead>
            <tbody>

            <%
                List<Coordinates> pointsList = CoordinatesBean.getPoints();

                ListIterator<Coordinates> points = pointsList.listIterator(pointsList.size());

                while (points.hasPrevious()) {
                    Coordinates coordinates = points.previous();
            %>
            <tr class="<%= coordinates.isHit() ? "hit" : "lose" %>">
                <td><%= coordinates.getX() %></td>
                <td><%= coordinates.getY() %></td>
                <td><%= coordinates.getR() %></td>
                <td><%= coordinates.isHit() ? "Да" : "Нет" %></td>
                <td><%= coordinates.getExecutionTimeMS() %></td>
                <td><%= coordinates.getCurrentTime() %></td>
            </tr>
            <% } %>
            </tbody>
        </table>

    </div>


</div>
<script src="jquery-3.6.0.min.js"></script>
<script src="script.js"></script>
</body>
</html>